package mx.iteso.desi.cloud.lp1;

import java.util.Set;
import java.util.Iterator;
import java.net.UnknownHostException;
import java.util.HashSet;
import mx.iteso.desi.cloud.keyvalue.KeyValueStoreFactory;
import mx.iteso.desi.cloud.keyvalue.PorterStemmer;
import mx.iteso.desi.cloud.keyvalue.IKeyValueStorage;

public class QueryImages {
  IKeyValueStorage imageStore;
  IKeyValueStorage titleStore;
	
  public QueryImages(IKeyValueStorage imageStore, IKeyValueStorage titleStore) 
  {
	  this.imageStore = imageStore;
	  this.titleStore = titleStore;
  }
	
  public Set<String> query(String word)
  {
	  Set<String> titleMatches =  titleStore.get(PorterStemmer.stem(word));
	  Set<String> imageMatches = new HashSet<String>();
	  for(String s : titleMatches) {
		  for(String si : imageStore.get(s)) {
			  imageMatches.add(si);
			  System.out.println(si);
		  }
		  System.out.println(s);
	  }
    // TODO: Return the set of URLs that match the given word,
    //       or an empty set if there are no matches
	  return imageMatches;
  }
        
  public void close()
  {
    // TODO: Close the databases
	  imageStore.close();
	  titleStore.close();
  }
	
  public static void main(String args[]) 
  {
    // TODO: Add your own name here
    System.out.println("*** Alumno: _____________________ (Exp: _________ )");
    
    IKeyValueStorage imageStore;
	try {
		imageStore = KeyValueStoreFactory.getNewKeyValueStore(Config.storeType, 
				"images");
		IKeyValueStorage titleStore = KeyValueStoreFactory.getNewKeyValueStore(Config.storeType, 
				"terms");
		QueryImages myQuery = new QueryImages(imageStore, titleStore);

	    for (int i=0; i<args.length; i++) {
	      System.out.println(args[i]+":");
	      Set<String> result = myQuery.query(args[i]);
	      Iterator<String> iter = result.iterator();
	      while (iter.hasNext()) 
	        System.out.println("  - "+iter.next());
	    }
	    
	    myQuery.close();
	} catch (UnknownHostException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
  
    
  }
}

