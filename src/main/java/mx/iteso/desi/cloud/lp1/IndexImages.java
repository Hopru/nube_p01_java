package mx.iteso.desi.cloud.lp1;

import java.io.IOException;
import java.util.HashSet;
import mx.iteso.desi.cloud.keyvalue.IKeyValueStorage;
import mx.iteso.desi.cloud.keyvalue.KeyValueStoreFactory;
import mx.iteso.desi.cloud.keyvalue.ParseTriples;
import mx.iteso.desi.cloud.keyvalue.PorterStemmer;
import mx.iteso.desi.cloud.keyvalue.Triple;

public class IndexImages {
  ParseTriples parser;
  IKeyValueStorage imageStore, titleStore;
    
  public IndexImages(IKeyValueStorage imageStore, IKeyValueStorage titleStore) {
	  this.imageStore = imageStore;
	  this.titleStore = titleStore;
  }
      
  public void run(String imageFileName, String titleFileName) throws IOException
  {
    // TODO: This method should load all images and titles 
    //       into the two key-value stores.
	  ParseTriples imagesParser = new ParseTriples("D:\\nube\\p01_kvs\\images_en.ttl");
	  ParseTriples titlesParser = new ParseTriples("D:\\nube\\p01_kvs\\labels_en.ttl");
	 
	  Triple imageTriple = imagesParser.getNextTriple();
	  String subject = imageTriple.getSubject();
	  //imageTriple = null;
	  
	  while(imageTriple != null) {
		  if(imageTriple.get(1).endsWith("/depiction") && subject.substring(subject.lastIndexOf("/") + 1).toLowerCase().startsWith(Config.filter.toLowerCase())
				  && imageTriple.get(2) != "http://commons.wikimedia.org/wiki/Special:FilePath/Carver_County_Minnesota_Incorporated_and_Unincorporated_areas_Norwood_Young_America_Highlighted.svg"
					  ) {
			  System.out.println(imageTriple.get(0));
			  System.out.println(imageTriple.get(1));
			  System.out.println(imageTriple.get(2));
			  System.out.println("Inicio");
			  
			  
			  imageStore.addToSet(imageTriple.get(0), imageTriple.get(2));
			  subject = imageTriple.getSubject();
		  }
		  
		  imageTriple = imagesParser.getNextTriple();  
	  }
	  
	  Triple titleTriple = titlesParser.getNextTriple();
	  //titleTriple = null;
	  while(titleTriple != null) {
		  if(titleTriple.get(1).endsWith("#label") && imageStore.exists(titleTriple.get(0))) {
			  System.out.println(titleTriple.get(0));
			  System.out.println(titleTriple.get(1));
			  System.out.println(titleTriple.get(2));
			  System.out.println("Fin");
			  
			  for(String s : titleTriple.get(2).split("\\s*,*\\s")) {
				  String steamedString = PorterStemmer.stem(s);
				  System.out.println(s);;
				  
				  if(!steamedString.equals("Invalid term") && !steamedString.equals("No term entered")) {
					  //System.out.println(steamedString);
					  titleStore.addToSet(steamedString.toLowerCase(), titleTriple.get(0));
				  }
			  }
		  }
		  
		  titleTriple = titlesParser.getNextTriple();
	  }
	  
	  
	  imagesParser.close();
	  titlesParser.close();
  }
  
  public void close() {
	  //TODO: close the databases;
	  imageStore.close();
	  titleStore.close();
  }
  
  public static void main(String args[])
  {
    // TODO: Add your own name here
    System.out.println("*** Alumno: _____________________ (Exp: _________ )");
    try {

      IKeyValueStorage imageStore = KeyValueStoreFactory.getNewKeyValueStore(Config.storeType, 
    			"images");
      IKeyValueStorage titleStore = KeyValueStoreFactory.getNewKeyValueStore(Config.storeType, 
  			"terms");
      IndexImages indexer = new IndexImages(imageStore, titleStore);
      //imageStore.addToSet("hola", "adios");
      //imageStore.get("0");
      
      //IndexImages indexer = new IndexImages(imageStore, titleStore);
      indexer.run(Config.imageFileName, Config.titleFileName);
      System.out.println("Indexing completed");
      indexer.close();
      
      
    } catch (Exception e) {
      e.printStackTrace();
      System.err.println("Failed to complete the indexing pass -- exiting");
    }
  }
}

