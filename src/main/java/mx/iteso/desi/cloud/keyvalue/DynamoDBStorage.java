package mx.iteso.desi.cloud.keyvalue;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

//Create table
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.CreateTableResult;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;


//Get information for table
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughputDescription;
import com.amazonaws.services.dynamodbv2.model.TableDescription;

//Add Item
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

//Get item
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import java.util.HashMap;
import java.util.Map;

import mx.iteso.desi.cloud.lp1.Config;

public class DynamoDBStorage extends BasicKeyValueStore {

    static AmazonDynamoDB ddb = AmazonDynamoDBClientBuilder.standard().withRegion(Config.amazonRegion).build();
    static DynamoDB dynamoDB = new DynamoDB(ddb);
    String dbName;

    // Simple autoincrement counter to make sure we have unique entries
    int inx;

    static Set<String> attributesToGet = new HashSet<String>();

    public DynamoDBStorage(String dbName) {
        this.dbName = dbName;
    	try {
    	    TableDescription table_info =
    	       ddb.describeTable(dbName).getTable();

    	    if (table_info != null) {
    	        System.out.format("Table name  : %s\n",
    	              table_info.getTableName());
    	        System.out.format("Table ARN   : %s\n",
    	              table_info.getTableArn());
    	        System.out.format("Status      : %s\n",
    	              table_info.getTableStatus());
    	        System.out.format("Item count  : %d\n",
    	              table_info.getItemCount().longValue());
    	        System.out.format("Size (bytes): %d\n",
    	              table_info.getTableSizeBytes().longValue());

    	        ProvisionedThroughputDescription throughput_info =
    	           table_info.getProvisionedThroughput();
    	        System.out.println("Throughput");
    	        System.out.format("  Read Capacity : %d\n",
    	              throughput_info.getReadCapacityUnits().longValue());
    	        System.out.format("  Write Capacity: %d\n",
    	              throughput_info.getWriteCapacityUnits().longValue());

    	        List<AttributeDefinition> attributes =
    	           table_info.getAttributeDefinitions();
    	        System.out.println("Attributes");
    	        for (AttributeDefinition a : attributes) {
    	            System.out.format("  %s (%s)\n",
    	                  a.getAttributeName(), a.getAttributeType());
    	        }
    	        
    	        
    	    }
    	    else {
    	    	
    	    }
    	} catch (AmazonServiceException e) {
    	    System.err.println(e.getErrorMessage());
    	    System.out.println("Creating table");
    	    CreateTableRequest request = new CreateTableRequest()
            	    .withAttributeDefinitions(
            	          new AttributeDefinition("mykey", ScalarAttributeType.S),
            	          new AttributeDefinition("inx", ScalarAttributeType.N))
            	    .withKeySchema(
            	          new KeySchemaElement("mykey", KeyType.HASH),
            	          new KeySchemaElement("inx", KeyType.RANGE))
            	    .withProvisionedThroughput(
            	          new ProvisionedThroughput(new Long(1), new Long(1)))
            	    .withTableName(dbName);
    	    try {
                CreateTableResult result = ddb.createTable(request);
                System.out.println(result.getTableDescription().getTableName());
            } catch (AmazonServiceException ex) {
                System.err.println(ex.getErrorMessage());

            }
    	}
       
    }
    
    @Override
    public Set<String> get(String search) {
    	
    	Set<String> ret = new HashSet<String>();
		Table table = dynamoDB.getTable(this.dbName);

        QuerySpec spec = new QuerySpec()
                .withKeyConditionExpression("mykey = :v_id")
                .withValueMap(new ValueMap().withString(":v_id", search));
        ItemCollection<QueryOutcome> items = table.query(spec);

//        System.out.println("\nfindRepliesInLast15DaysWithConfig results:");
        Iterator<Item> iterator = items.iterator();
        while (iterator.hasNext()) {
        	String val = iterator.next().getString("myvalue");
            ret.add(val);
        }
        return ret;
    }

    @Override
    public boolean exists(String search) {
    	if(attributesToGet.contains(search)) {
    		return true;
    	} else {
    		return false;
    	}
    }

    @Override
    public Set<String> getPrefix(String search) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addToSet(String keyword, String value) {
    	HashMap<String,AttributeValue> item_values =
    		    new HashMap<String,AttributeValue>();

    		item_values.put("inx", new AttributeValue().withN("" + this.inx));
    		item_values.put("mykey", new AttributeValue().withS(keyword));
    		item_values.put("myvalue", new AttributeValue().withS(value));
  
    		try {
    		    ddb.putItem(this.dbName, item_values);
    		    attributesToGet.add(keyword);
    		    this.inx++;
    		} catch (ResourceNotFoundException e) {
    		    System.err.format("Error: The table \"%s\" can't be found.\n", this.dbName);
    		    System.err.println("Be sure that it exists and that you've typed its name correctly!");

    		} catch (AmazonServiceException e) {
    		    System.err.println(e.getMessage());
    	}
    }

    @Override
    public void put(String keyword, String value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public boolean supportsPrefixes() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void sync() {
    }

    @Override
    public boolean isCompressible() {
        return false;
    }

    @Override
    public boolean supportsMoreThan256Attributes() {
        return true;
    }

}
